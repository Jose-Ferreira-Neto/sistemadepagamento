const mongoose = require("mongoose")
const Schema = mongoose.Schema;

const cadastro = new Schema({
    nome: {
        type: String,
        required: true,
        default: "NÃO INFORMADO"
    },
    Sobrenome: {
        type: String,
        required: true,
        default: "NÃO INFORMADO"
    },
    email: {
        type: String,
        required: true,
        default: "NÃO INFORMADO"
    
    },
    senha:{
        type: String,
        required: true,
        default: "NÃO INFORMADO"
    },
    cpf: {
        type: Number,
        require: true,
        default: "NÃO INFORMADO"
    },
    nome_chamada:{
        type: String,
        required: true,
        default: "NÃO INFORMADO"
    },
    nome_picpay:{
        type: String,
        required: true,
        default: "NÃO INFORMADO"
    },
    estatus:{
        type: String,
        required: true,
        default: "NÃO INFORMADO"
    },
    parcelas_pagas:{
        type: String,
        required: true,
        default: "NÃO INFORMADO"
    },
    data:{
        type: Date,
        required: true,
        default: Date.now()
    }

})

mongoose.model("cadastro", cadastro)
